# 2048-game-deployment

### Локальное тестирование
Следует установить `node 16`, согласно [инструкции](https://www.stewright.me/2022/01/tutorial-install-nodejs-16-on-ubuntu-20-04/)

```shell
curl -s https://deb.nodesource.com/setup_16.x | sudo bash
sudo apt install nodejs -y
```

Склонировать репозиторий и мерейти в папку `2048-game`:

```shell
git clone https://gitlab.com/conchis-group/projects/2048-game-deployment.git
cd 2048-game-deployment/2048-game
```

Запусть приложение посредством команд:

```shell
npm install --include=dev
npm run build
npm start
```

Оно будет доступно на localhost:8080


### Развертывание контейнеризированного приложения с помощью Ansible

Ansible роль по установке docker вынесена в отдельный [репозиторий](https://gitlab.com/conchis-group/ansible-roles/ansible-role-docker). Include осуществляется посредством [ansible-galaxy](https://docs.ansible.com/ansible/latest/cli/ansible-galaxy.html) и файла `requirements.yml`:

```shell
ansible-galaxy install -r requirements.yml
```

Развертывание осуществляется в рамках виртуальной машины с использованием [Vagrant](https://www.vagrantup.com/). Учитывая, что Ansible является одним из многих программ, интегрированных с Vagrant, для запуска ВМ с последующим провижингом достаточно выполнить команду:

```shell
vagrant up
```

### Deploy в k8s кластер (Yandex Cloud) с помощью Helm Chart

- Список репозиториев:
    - [Helm chart](https://gitlab.com/conchis-group/helm-charts/2048-game) - для чарта приложения оформлен pipeline для автоматической публикации в [package registry](https://docs.gitlab.com/ee/user/packages/helm_repository/). 
    - [k8s-connector](https://gitlab.com/conchis-group/k8s-connection) - настройки для GitLab agent`а для всех проектов группы conchis-group/projects. Реализован согласно [инструкции](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html).

- Инфраструктура подготавливается через Terraform. [Инструкция](https://cloud.yandex.ru/ru/docs/tutorials/infrastructure-management/terraform-quickstart) по началу работы с Terraform от Yandex Cloud.

- Развертывание осуществляется через стандартный terraform workflow:
    ```shell
    cd ./terraform
    terraform init
    terrafoem apply -lock=false 
    ```
- Перед деплоем необходимо заполнить файл `env.sh` актуальными даннымии и выполнить команду `source ./env.sh`.

- Для подключение kubernetes-кластера к GitLab следует установить GitLab Agent в рамках одной из нод. Для этого можно воспользоваться [решением от Yandex Cloud`а](https://cloud.yandex.ru/ru/marketplace/products/yc/gitlab-agent) или следовать [инструкции от GitLab](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#register-the-agent-with-gitlab).
