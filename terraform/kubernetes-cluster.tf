resource "yandex_kubernetes_cluster" "k8s-cluster" {
  name        = "k8s-cluster"

  network_id = yandex_vpc_network.k8s-network.id

  master {
    version = "1.28"
    zonal {
      zone      = var.yc_zone
      subnet_id = yandex_vpc_subnet.k8s-subnet.id
    }

    public_ip = true

    maintenance_policy {
      auto_upgrade = true
    }
  }

  service_account_id      = var.service_account_id
  node_service_account_id = var.service_account_id

  release_channel = "RAPID"
  network_policy_provider = "CALICO"
}
