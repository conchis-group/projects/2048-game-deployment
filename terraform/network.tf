resource "yandex_vpc_network" "k8s-network" {
  name = "k8s-network"
}

resource "yandex_vpc_subnet" "k8s-subnet" {
  name           = "k8s-subnet"
  v4_cidr_blocks = ["192.168.10.0/24"]
  zone           = var.yc_zone
  network_id     = yandex_vpc_network.k8s-network.id
}
