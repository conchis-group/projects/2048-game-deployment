variable "yc_folder_id" {
        description = "Folder id"
        type        = string
}

variable "yc_bucket_name" {
        description = "Name of yandex cloud bucket"
        type        = string
}

variable "yc_access_key" {
        type        = string
}

variable "yc_secret_key" {
        type        = string
}

variable "yc_cloud_id" {
        type        = string
}

variable "yc_token" {
        type        = string
}

variable "yc_zone" {
        type        = string
        default     = "ru-central1-a"
}

variable "service_account_id" {
	type        = string
}

variable "ssh_key_path" {
	description = "Path to ssh pub key"
  type        = string
  default     = "~/.ssh/id_ed25519.pub"
}
