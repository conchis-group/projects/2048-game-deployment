FROM node:latest AS builder
WORKDIR /app
COPY ./2048-game ./
RUN npm install --include=dev
RUN npm run build

FROM node:16-alpine
WORKDIR /app
COPY ./2048-game ./
RUN npm install --include=dev
COPY --from=builder /app/dist ./dist
CMD ["npm", "start"]
